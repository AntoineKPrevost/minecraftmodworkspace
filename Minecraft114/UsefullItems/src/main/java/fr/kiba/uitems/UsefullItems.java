package fr.kiba.uitems;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

import java.util.logging.Logger;

@Mod(UsefullItems.MODID)
public class UsefullItems {

    public static final String MODID = "uitems";

    public static final Logger logger = Logger.getLogger(MODID);

    public UsefullItems(){
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientSetup);
    }

    private void setup(final FMLCommonSetupEvent event){
        logger.info("Mod Setup chargé");
    }

    private void clientSetup(final FMLClientSetupEvent event){
        logger.info("Mod Client Setup chargé");
    }
    
}
